const axios = require("axios")
require('dotenv').config({ path: './.env' });

GITLAB_URL="https://gitlab.com/"
GITLAB_API="https://gitlab.com/api/v4"
GROUP_ID="65792029"
CICD_RUNNER_TOKEN=process.env.EXTISM_RUNNERS_TOKEN


const gitlabAPI = GITLAB_API
const groupId = GROUP_ID

let headers = {
  "Content-Type": "application/json",
  "Private-Token": process.env.GITLAB_TOKEN_ADMIN
}

let query = ({method, path, headers, data}) => {
  return axios({
    method: method,
    url: `${gitlabAPI}/${path}`,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}

let runnersList = ({groupId, headers, tag}) => query({
  method: "GET", 
  path: `groups/${groupId}/runners?tag_list=${tag}`,
  headers: headers
})

let removeRunner = ({runnerId, headers}) => query({
  method: "DELETE", 
  path: `runners/${runnerId}`,
  headers: headers
})


runnersList({
  groupId: groupId,
  headers: headers,
  tag: "gitpod"
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
 
  runners.forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})

runnersList({
  groupId: groupId,
  headers: headers,
  tag: "gitpod-alpine"
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
 
  runners.forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})

runnersList({
  groupId: groupId,
  headers: headers,
  tag: "gitpod-extism"
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
 
  runners.forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})

runnersList({
  groupId: groupId,
  headers: headers,
  tag: "gitpod-node-js"
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
 
  runners.forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})

runnersList({
  groupId: groupId,
  headers: headers,
  tag: "gitpod-modsurfer"
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
 
  runners.forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})
