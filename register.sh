#!/bin/bash

GITLAB_URL="https://gitlab.com/"
GITLAB_API="https://gitlab.com/api/v4"
GROUP_ID="65792029"
CICD_RUNNER_TOKEN="${EXTISM_RUNNERS_TOKEN}"

docker run --rm -it -v gitlab-runner01-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 Gitpod alpine" \
  --tag-list "gitpod-alpine" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner02-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 Gitpod alpine" \
  --tag-list "gitpod-alpine" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner03-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 Gitpod alpine" \
  --tag-list "gitpod-alpine" \
  --run-untagged="true" \
  --locked="false"

# Extism runner
docker run --rm -it -v gitlab-runner04-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image registry.gitlab.com/wasmkitchen/extism/extism-tools/extism-golang:0.0.0 \
  --description "💀 Extism runner" \
  --tag-list "gitpod-extism" \
  --run-untagged="false" \
  --locked="false"

# Extism nodejs
docker run --rm -it -v gitlab-runner05-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image node:alpine \
  --description "🟩 Node.js runner" \
  --tag-list "gitpod-node-js" \
  --run-untagged="false" \
  --locked="false"


# Gitpod full
docker run --rm -it -v gitlab-runner06-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image gitpod/workspace-full \
  --description "🍊 Gitpod runner" \
  --tag-list "gitpod" \
  --run-untagged="true" \
  --locked="false"


